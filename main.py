from cbc_rs import pad, unpad, cbc_encrypt, cbc_decrypt

if __name__ == '__main__':
    key = b'\0' * 16
    iv = 0
    message = b'YELLOW SUBMARINE'
    padded = pad(message)
    print(padded)
    ciphertext = cbc_encrypt(key, iv, padded)
    print(ciphertext)
    deciphered = cbc_decrypt(key, iv, ciphertext)
    print(deciphered)
    unpadded = unpad(deciphered)
    print(unpadded)
