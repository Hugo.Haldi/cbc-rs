# CBC-RS

A simple implementation of a CBC (Cipher Block Chaining) in Rust using AES128

## Concept

Currently, the default key and IV are both 16 bytes at zero. It will try to encrypt the plaintext _YELLOW SUBMARINE_ with padding added.

### Padding

The padding adds the number of missing bytes $p$ to fill a full block (of 16 bytes by default) at the end of the message, repeated $p$ times. I.e. we have a message of length $n$ and blocks size of $b$, then $n + p = \left\lceil \frac{n}{b} \right \rceil \cdot b$, so we can get $p$ with $p = b - (n \mod b)$

### Usage

```bash
$ cargo run
[159, 150, 106, 206, 236, 232, 71, 205, 51, 51, 187, 15, 213, 48, 97, 114, 32, 208, 217, 203, 43, 0, 96, 141, 232, 227, 13, 87, 247, 197, 129, 152]
"YELLOW SUBMARINE"
```

Cargo is the Rust build tool. `cargo run` will compile the code and execute it. The first line is the ciphertext vector, encrypted with CBC using AES128, the given key and IV. Then the second line is the deciphered message, which corresponds to the plaintext.

If there is an error in the padding, the algorithm will show an error. This will be useful to performs _Padding oracle_ attacks.
