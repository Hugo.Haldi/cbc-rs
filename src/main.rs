use cbc_rs::{cbc_decrypt, cbc_encrypt, pad, unpad};

fn main() {
    let m = Vec::from("YELLOW SUBMARINE");
    let iv = 0u128;
    let key = [0u8; 16];

    let m = pad(&m);
    let ciphertext = cbc_encrypt(&key, iv, &m);

    println!("{:?}", ciphertext);

    let deciphered = cbc_decrypt(&key, iv, &ciphertext);

    match unpad(&deciphered) {
        Ok(m) => println!("{:?}", String::from_utf8(m.to_vec()).unwrap()),
        Err(e) => println!("{}", e),
    }

    // println!("{:?}", String::from_utf8(deciphered).unwrap());
}
