use std::borrow::Cow;

use aes::{
    cipher::{generic_array::GenericArray, typenum::U16, BlockDecrypt, BlockEncrypt, KeyInit},
    Aes128,
};

use pyo3::{exceptions::PyValueError, prelude::*};

const BLOCK_SIZE: u8 = 16;

#[pyfunction]
pub fn pad(m: &[u8]) -> Cow<[u8]> {
    let padding_size = BLOCK_SIZE - u8::try_from(m.len() % usize::from(BLOCK_SIZE)).unwrap();
    let padding = vec![padding_size; padding_size.into()];
    return [m, &padding].concat().into();
}

#[pyfunction]
pub fn unpad(m: &[u8]) -> PyResult<Cow<[u8]>> {
    let padding_size = *m.last().unwrap();
    if usize::from(padding_size) > m.len() {
        return Err(PyValueError::new_err("Padding error"));
    }

    for e in &m[m.len() - usize::from(padding_size)..] {
        if *e != padding_size {
            return Err(PyValueError::new_err("Padding error"));
        }
    }

    return Ok(m[..m.len() - usize::from(padding_size)].into());
}

#[pyfunction]
pub fn cbc_encrypt<'a>(key: &[u8], iv: u128, m: &[u8]) -> Cow<'a, [u8]> {
    let mut tmp = iv;
    let mut ciphertext = Vec::<u8>::new();

    let cipher = Aes128::new(GenericArray::from_slice(key));

    for b in m.chunks(16) {
        let a = u128::from_be_bytes(b.try_into().unwrap());
        let mut cipherblock: GenericArray<_, U16> = GenericArray::from((a ^ tmp).to_be_bytes());
        cipher.encrypt_block(&mut cipherblock);
        tmp = u128::from_be_bytes(cipherblock.into());

        ciphertext.append(&mut Vec::from(cipherblock.as_mut_slice()));
    }

    return ciphertext.into();
}

#[pyfunction]
pub fn cbc_decrypt<'a>(key: &[u8], iv: u128, ciphertext: &[u8]) -> Cow<'a, [u8]> {
    let mut tmp = iv;
    let mut decipher = Vec::<u8>::new();

    let cipher = Aes128::new(GenericArray::from_slice(key));

    for b in ciphertext.chunks(16) {
        let mut cipherblock: GenericArray<_, U16> = GenericArray::clone_from_slice(b);
        cipher.decrypt_block(&mut cipherblock);

        let a = u128::from_be_bytes(cipherblock.try_into().unwrap());
        decipher.append(&mut Vec::from((a ^ tmp).to_be_bytes()));
        tmp = u128::from_be_bytes(b.try_into().unwrap());
    }

    return decipher.into();
}

// pub fn pad(m: &mut Vec<u8>) {
//     let padding_size = BLOCK_SIZE - u8::try_from(m.len() % usize::from(BLOCK_SIZE)).unwrap();
//     m.append(&mut vec![padding_size; padding_size.into()]);
// }

// pub fn unpad(m: &mut Vec<u8>) -> Result<(), String> {
//     let padding_size = *m.last().unwrap();
//     if usize::from(padding_size) > m.len() {
//         return Err(String::from("Padding error"));
//     }

//     for e in &m[m.len() - usize::from(padding_size)..] {
//         if *e != padding_size {
//             return Err(String::from("Padding error"));
//         }
//     }

//     *m = m[..m.len() - usize::from(padding_size)].to_vec();
//     return Ok(());
// }

// pub fn cbc_encrypt(cipher: &Aes128, iv: u128, m: &Vec<u8>) -> Vec<u8> {
//     let mut tmp = iv;
//     let mut ciphertext = Vec::<u8>::new();

//     for b in m.chunks(16) {
//         let a = u128::from_be_bytes(b.try_into().unwrap());
//         let mut cipherblock: GenericArray<_, U16> = GenericArray::from((a ^ tmp).to_be_bytes());
//         cipher.encrypt_block(&mut cipherblock);
//         tmp = u128::from_be_bytes(cipherblock.into());

//         ciphertext.append(&mut Vec::from(cipherblock.as_mut_slice()));
//     }

//     return ciphertext;
// }

// pub fn cbc_decrypt(cipher: &Aes128, iv: u128, ciphertext: &Vec<u8>) -> Vec<u8> {
//     let mut tmp = iv;
//     let mut decipher = Vec::<u8>::new();

//     for b in ciphertext.chunks(16) {
//         let mut cipherblock: GenericArray<_, U16> = GenericArray::clone_from_slice(b);
//         cipher.decrypt_block(&mut cipherblock);

//         let a = u128::from_be_bytes(cipherblock.try_into().unwrap());
//         decipher.append(&mut Vec::from((a ^ tmp).to_be_bytes()));

//         tmp = u128::from_be_bytes(b.try_into().unwrap());
//     }

//     return decipher;
// }

#[pymodule]
fn cbc_rs(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(pad, m)?)?;
    m.add_function(wrap_pyfunction!(unpad, m)?)?;
    m.add_function(wrap_pyfunction!(cbc_encrypt, m)?)?;
    m.add_function(wrap_pyfunction!(cbc_decrypt, m)?)?;
    Ok(())
}
